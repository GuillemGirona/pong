/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, LOAD, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    bool pause=false;
    Rectangle vida1={100,10,100,20};
    Rectangle vida2={screenWidth-180,10,100,20};
    int puntos1 = 10;
    int puntos2 = 10;
    int direx = 0;
    bool start1 = false;
    bool start2 = false;
    int salir = 0;
    int contadorsegundos = 99;
    int cuenta = 0;
    int framesball = 0;
    int lastouch = 0;
    int direy = 0;
    int size = 20;
    int boxsizeY = 100;
    int boxsizeX = 40;
    int boxsize2Y = 100;
    int boxsize2X = 40;
    int boxPosition2Y =screenHeight/2;
    int boxPosition2X =  screenWidth-boxsizeX;
    int boxPositionY = screenHeight/2;
    int boxPositionX =0 ;
    Rectangle jug1 = { boxPositionX, boxPositionY, boxsizeX, boxsizeY };
    Rectangle jug2 = { boxPosition2X, boxPosition2Y, boxsize2X, boxsize2Y};
    

    
    GameScreen screen = GAMEPLAY;
    
    // TODO: Define required variables here..........................
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    int playerSpeedY;
    int boxpositionY = screenHeight/2 - 40;
    
    Rectangle enemy;
    int enemySpeedY;
    
    Vector2 ballPosition = { screenWidth/2, screenHeight/2 };
    Vector2 ballSpeed = {0.2};
    float ballRadius = 20;
    
    
    int ax = 4;
    int ay = 8;
    
    
    int playerLife;
    int enemyLife;
    int acelerar = 5;
    
    Rectangle rec = {175,250, 0, 50};
    
    Rectangle barEnemy = { 760, 200, 40, 110 };
    int visionEnemy = screenWidth - 500;
    int speedEnemy = 2;
    
    int secondsCounter = 99;
    int resto=99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    const char msgTtf[64] = "PLA";
    const char msgTtf4[64] = "YER 1";
    const char msgTtf2[64] = "YER 2";
    const char msgTtf3[64] = "PLA";
    const char msgTtf5[64] = "THE LAST PONG";
    const char msgTtf6[64] = "PRESS ENTER";
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    Image image = LoadImage("FONDO.png");
    Image image2 = LoadImage("LOGO.png");
    Image image3 = LoadImage("gameover.png");
    Texture2D texture = LoadTextureFromImage(image);
    Texture2D texture2 = LoadTextureFromImage(image2);
    Texture2D texture3 = LoadTextureFromImage(image3);
    UnloadImage(image); 
    SpriteFont fontTtf = LoadSpriteFont("Instructions.ttf");
    SpriteFont font = LoadSpriteFont("Pong.ttf");
    
    int fadeout = 0;
    float alpha = 0.0f;
    int pantcount = 0;
    
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............
                 framesCounter++;
            if(framesCounter>=5){
                if(fadeout==0){
                    alpha+=0.1f;
                    framesCounter=0;
                }
                if(alpha>=1.0f){
                    fadeout=1;
                }
                if(fadeout==1){
                    pantcount++;
                    
                    if(pantcount>=90){
                      alpha-=0.1f;
                      framesCounter=0;
                      
                    if(alpha<=0.0f){
                    fadeout=0;
                    framesCounter=0;
                    
                    screen=TITLE;
                    }
                }
                }
             }
                
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................
                
                // TODO: "PRESS ENTER" logic.........................
                if(IsKeyDown(KEY_ENTER)){
                screen=LOAD;
                }
                
                
            } break;
                        
             case LOAD: 

            {
            
            if(rec.width<=400){
             rec.width+=3;
            }else{
            rec.width-=3;
            screen=GAMEPLAY;
            }
            
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                
            if(pause == false){
            HideCursor();
            framesCounter++;
            framesball++;
            cuenta++;
            if(cuenta>=60){
            resto--;
            cuenta=0;
            }


                // TODO: Ball movement logic.........................
                
                if (player.y <= 0) player.y = 0;
                else if ((player.y + player.height) >= screenHeight) player.y = screenHeight - player.height;
                
                if(pause==false){
                
                ballPosition.x += ax;
                ballPosition.y += ay;
       
                if(ballPosition.x<ballRadius){
                ax *= -1;
                }
       
                if(ballPosition.y<ballRadius){
                ay *= -1;
                }
       
                if(ballPosition.x>screenWidth-ballRadius){
                ax *= -1;
                }
       
                if(ballPosition.y>screenHeight-ballRadius){
                ay *= -1;
                }
                                
                // TODO: Player movement logic.......................
                
                if(IsKeyDown(KEY_UP)){
                boxpositionY=boxpositionY-acelerar;
                }
                if (IsKeyDown(KEY_DOWN)){
                boxpositionY=boxpositionY+acelerar;
                }
                

                
                // TODO: Enemy movement logic (IA)...................
                
        framesCounter++;
        
        if ((framesCounter%60) == 0) secondsCounter++;
        
        ballPosition.x += ballSpeed.x;
        ballPosition.y += ballSpeed.y;
        
        if (((ballPosition.x + ballRadius) >= screenWidth) || ((ballPosition.x - ballRadius) <= 0)) ballSpeed.x *= -1;
        if (((ballPosition.y + ballRadius) >= screenHeight) || ((ballPosition.y - ballRadius) <= 0)) ballSpeed.y *= -1;

        if (ballPosition.x >= visionEnemy)
        {
            if (ballPosition.y > (barEnemy.y + barEnemy.height/2)) barEnemy.y += speedEnemy;
            if (ballPosition.y < (barEnemy.y + barEnemy.height/2)) barEnemy.y -= speedEnemy;
        }
        
        if (barEnemy.y <= 0) barEnemy.y = 0;
        else if ((barEnemy.y + barEnemy.height) >= screenHeight) barEnemy.y = screenHeight - barEnemy.height;
        
        if (IsKeyDown(KEY_RIGHT)) visionEnemy += 10;
        if (IsKeyDown(KEY_LEFT)) visionEnemy -= 10;
        
        if (CheckCollisionCircleRec(ballPosition, ballRadius, barEnemy))
        {
            ballSpeed.x *= -1;
        }

                
                // TODO: Collision detection (ball-player) logic.....

                if(CheckCollisionCircleRec(ballPosition, ballRadius, barEnemy))
                {
                if(ballSpeed.x < 0){
                    ballSpeed.x *= -1;
                }
                }
                if(CheckCollisionCircleRec(ballPosition, ballRadius, rec))
                {
                if(ballSpeed.x > 0){
                    ballSpeed.x *= -1;
                }
                }
                
                if(rec.y<0){
                    rec.y=0;
                }
                if(barEnemy.y<0){
                    barEnemy.y=0;
                }
                if(rec.y>screenHeight-rec.height){
                    rec.y=screenHeight-rec.height;
                }
                if(barEnemy.y>screenHeight-barEnemy.height){
                    barEnemy.y=screenHeight-barEnemy.height;
                }
                
                
                // TODO: Collision detection (ball-enemy) logic......
                
                // TODO: Collision detection (ball-limits) logic.....
                
                // TODO: Life bars decrease logic....................
                
                if(ballPosition.x>screenWidth-20){ 
                puntos2--;
                vida2.width -= 10;
                direx = 0;
                start2=true;
                }
                if(ballPosition.x<20){
                puntos1--;
                vida1.width -= 10;
                direx = 1;
                start1=true;
                }

                // TODO: Time counter logic..........................

                // TODO: Game ending logic...........................
                }
            }
                  if(puntos1<=0 || puntos2<=0 || resto<=0){
                  screen=ENDING;
              }
                
                // TODO: Pause button logic..........................
                
                    if(IsKeyPressed(KEY_ENTER)){
                    pause=!pause;
                }
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................
                
                
                if(IsKeyPressed(KEY_ENTER)){
                    salir = 1;
                }
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................
                    
                    DrawTexture(texture2, screenWidth/2 - texture2.width/2, screenHeight/2 - texture2.height/2, WHITE);
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................
                    
                    DrawTextEx(font, msgTtf5, (Vector2){210,100}, font.baseSize, 20, BLUE);
                    
                    // TODO: Draw "PRESS ENTER" message..............
                    
                    DrawTextEx(fontTtf, msgTtf6, (Vector2){275,300}, fontTtf.baseSize, 10, BLUE);
                    
                } break;
                case LOAD :
                {
                    DrawRectangleRec(rec, GREEN);
                    DrawText("CARGANDO...", 220, 200, 50, BLACK);
                    
                } break; 
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                    ClearBackground(RAYWHITE);
            DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, WHITE);
            DrawTextEx(fontTtf, msgTtf, (Vector2){350,15}, fontTtf.baseSize, 0, BLUE);
            DrawTextEx(fontTtf, msgTtf2, (Vector2) {397,445}, fontTtf.baseSize, 0, RED);
            DrawTextEx(fontTtf, msgTtf3, (Vector2) {350,445}, fontTtf.baseSize, 0, BLUE);
            DrawTextEx(fontTtf, msgTtf4, (Vector2){397,15}, fontTtf.baseSize, 0, RED);
            DrawRectangle(0, boxpositionY, 40, 110, BLACK);
            DrawCircle(ballPosition.x, ballPosition.y, ballRadius, BLACK); 
                    
                    // TODO: Draw player and enemy...................
                    
                    DrawRectangleRec(barEnemy, BLACK);
                    DrawLine(screenWidth/2, 0, screenWidth/2, screenHeight, GRAY);
                    DrawLine(visionEnemy, 0, visionEnemy, screenHeight, RED);
                    
                    // TODO: Draw player and enemy life bars.........
                    
              DrawRectangleRec(vida1, BLACK);  
              DrawRectangleRec(vida2, WHITE);
                    
                    // TODO: Draw time counter.......................
                    
                    // TODO: Draw pause message when required........
                    
              if(pause == 1)
              {DrawTextEx(fontTtf, "PAUSE", (Vector2){175,230}, 200, 0, BLACK);
              }
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......
                    
                    if(puntos1 == 0){
                        DrawText("HAS PERDIDO ERES MALISIMO", 110, 45, 40, WHITE);
                    }
                    if(puntos2 == 0){
                        DrawText("EREES LA CAÑAA!!", 110, 45, 40, WHITE);
                    }
                    if(salir == 1){
                    screen=TITLE;
                    }
                    
                    DrawTexture(texture3, screenWidth/2 - texture3.width/2, screenHeight/2 - texture3.height/2, WHITE);
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadSpriteFont(fontTtf);
    UnloadTexture(texture);
    UnloadTexture(texture2);
    UnloadTexture(texture3);
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
    }